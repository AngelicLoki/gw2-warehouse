import gw2_price_refresh.function_main as main


def http_entrypoint(request):
    """
    The entrypoint for the Cloud Function lambda when coming from an http request
    :param request:
    :return:
    """
    main.main_method()
    
    # Cloud Functions are required to return a string (or list/dict, but not bool) to avoid a 500 return error. 
    #   That's why this isn't just True.
    return "true"


def pubsub_entrypoint(event, context):
    """
    The entrypoint for the Cloud Function lambda when coming from pubsub
    :param event:
    :param context:
    :return:
    """
    print("Starting cloud function")
    main.main_method()


if __name__ == '__main__':
    http_entrypoint(None)
