"""
Setup tools for the function in GW2 Warehouse that will periodically refresh the trading post price of items.

See https://gitlab.com/AngelicLoki/gw2-warehouse/
"""
from setuptools import setup

setup(
    name='gw2_price_refresh',
    version='0.0.2',
    url='https://gitlab.com/AngelicLoki/gw2-warehouse/',
    author='AngelicLoki',
    classifiers=[
        'Programming Language :: Python :: 3'
    ],
    python_requires='>=3.5, <4',

    # Which modules to package
    packages=[
        "gw2_price_refresh",
        "gw2_price_refresh/gw2api",
        "gw2_price_refresh/utils"
    ],

    data_files=[
        ("gw2_price_refresh", ["requirements.txt", "main.py"])
    ],

    # Dependency management for other stages
    extras_require={
        'dev': ['check-manifest'],
        'test': ['coverage'],
    },

    # Dependency Management!
    install_requires=[
        'wheel',
        'requests',
        'firebase-admin'
    ]
)
