"""
A utility module for handling firestore operations.

author: Angelic Loki
"""
import firebase_admin
import os
from firebase_admin import firestore

# The client to use for accessing firestorelist
_firestore_client = None


def put_item_data_to_db(item_data: dict):
    """
    Accepts a dictionary, then puts it into the Firestore database
    :param item_data: The data to put into the database
    :return: A boolean True if the data was input successfully
    """
    get_db_connection("gw2-warehouse") \
        .collection(u'items') \
        .document(str(item_data.get("id"))) \
        .set(item_data)

    return True


def get_meta_item_attributes() -> dict:
    """
    Retrieves information related to the last parse run and the maximum item # that was reached.
    :return: a dict object with metadata inside it
    """
    meta_attrs = get_db_connection("gw2-warehouse") \
        .collection(u'meta')

    toReturn = None
    for doc in meta_attrs.stream():
        toReturn = doc

    return toReturn.to_dict()


def put_meta_attributes(attributes: dict):
    """
    Convenience method for setting meta data attributes
    :param attributes: the meta attributes to set
    :return:
    """
    get_db_connection("gw2-warehouse") \
        .collection(u'meta') \
        .document(u'meta-attributes') \
        .set(attributes)

    return True


def get_db_connection(project_id: str) -> firestore.Client:
    """
    Retrieves a connection to the firestore database
    :param is_cloud: whether or not we're running in a cloud environment
    :param project_id: the project ID of our GCP account
    :return:
    """

    # Since we're going to modify the value here, define as global
    global _firestore_client

    # If we've already initialized a connection, no need to do so again. Short circuit.
    if _firestore_client is not None:
        return _firestore_client

    # Check if there is a credential file present. If there is, we'll use that. If not, we'll fall
    # back on the cloud IAM role. We won't deploy a credential file to GCP, so it will always fall
    # back to IAM, which is what we want.
    if os.path.exists('gcp_account.json'):
        credentials = firebase_admin.credentials.Certificate('gcp_account.json')
    else:
        credentials = firebase_admin.credentials.ApplicationDefault()

    # Initialize the connection using the project ID, and store the client globally.
    firebase_admin.initialize_app(credentials, {
        'projectId': project_id
    })
    _firestore_client = firestore.client()
    return _firestore_client
