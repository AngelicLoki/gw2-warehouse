import requests

_url_stub = "https://api.guildwars2.com/v2/"


def get_items(ids: range) -> dict:
    """
    Retrieve all items in a given range
    :param ids: list of items to get
    :return:
    """
    id_list = ','.join(map(str, list(ids)))
    response = requests.get(_url_stub+"items?ids="+id_list)
    to_return = response.json()
    return to_return


def get_item_ids() -> list:
    """
    Retrieve all items in a given range
    :param ids: list of items to get
    :return:
    """
    response = requests.get(_url_stub+"items")
    to_return = response.json()
    return to_return

