import requests

_url_stub = "https://api.guildwars2.com/v2/"


def get_prices_for_ids(ids: range):
    """
    Retrieve all prices in a given range
    :param ids: list of items to get
    :return:
    """
    id_list = ','.join(map(str, list(ids)))
    response = requests.get(_url_stub+"commerce/prices?ids="+id_list)
    to_return = response.json()
    return to_return
