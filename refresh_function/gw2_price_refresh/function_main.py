import gw2_price_refresh.utils.firestore_utils as firestore
import gw2_price_refresh.gw2api.items as gw2items
import gw2_price_refresh.gw2api.exchange as gw2exchange
import datetime
import os
import math

# The string format for all times
_str_format = "%Y-%m-%d_%H:%M:%S"

# item per page to retrieve
_page_count = 50

# How many times to loop before cutting off. Will also retrieve from
# the environment variable "LOOP_COUNT" at runtime
_loop_count = 30


def main_method():
    """
    The method that will run when the Cloud Function executes.
    :return: True if the method completed appropriately.
    """
    # Retrieve the metadata of the last run
    current_time = datetime.datetime.now().strftime(_str_format)
    meta_attrs = firestore.get_meta_item_attributes()

    # Retrieve loop count from environment variable, and overwrite if it's present
    global _loop_count
    if os.getenv("LOOP_COUNT") is not None:
        _loop_count = os.getenv("LOOP_COUNT")

    # If no metadata exists, populate it.
    if meta_attrs is None:
        meta_attrs = {'last_run': current_time, 'last_item': 0}
        firestore.put_meta_attributes(meta_attrs)

    final_item =  get_last_item_id()

    last_item = meta_attrs.get('last_item')
    item_range = range(last_item, last_item + _page_count)

    items_found = 0
    items_written = 0
    loop_count = 0
    while items_written < 250 \
            and item_range[0] < final_item \
            and loop_count < _loop_count:

        print(f"Updated {items_written} items to far, looping with first item id being {item_range[0]}, loop count {loop_count}")
        item_details = gw2items.get_items(item_range)
        item_prices = gw2exchange.get_prices_for_ids(item_range)

        # Check to ensure our dicts are valid
        if not is_api_return_valid(item_details, item_prices):
            item_range = range(item_range[-1], item_range[-1] + _page_count)

        # Execute the update loop
        items_written += update_item_prices(item_details, item_prices)

        # Setup the range for the next loop.
        item_range = range(item_range[-1], item_range[-1] + _page_count)
        loop_count += 1

    if item_range[-1] > final_item:
        update_meta_attr_after_run(meta_attrs=meta_attrs, last_processed_item=0)
    else:
        update_meta_attr_after_run(meta_attrs=meta_attrs, last_processed_item=item_range[-1])

    print("Updated {} items in the db, ending with item {}".format(items_written, meta_attrs.get("last_item")))
    return True


def update_item_prices(item_details: dict, item_prices: dict) -> int:
    """
    Loops over items in the 2 input dicts (zipping them together) and writes data back to Firestore.

    :param item_details:
    :param item_prices:
    :return:
        How many items are written from the input dicts.
    """
    items_written = 0

    # Now that we know both our returns are valid, zip them together an loop over them.
    for item, price in zip(item_details, item_prices):

        try:
            # TODO: Extract to method for testability
            flags = item.get('flags')
            # If we can't sell an item, no reason to index it right now. Skip it.
            if "NoSell" in flags or \
                    "SoulbindOnAcquire" in flags or \
                    "MonsterOnly" in flags:
                continue

            # build out our Firestore document
            # TODO: Extract to method for testability
            details = {}
            details.update(id=item.get('id'))
            details.update(name=item.get('name'))
            details.update(buy={'quantity': price.get('buys').get('quantity'),
                                'price': price.get('buys').get('unit_price')})
            details.update(sell={'quantity': price.get('sells').get('quantity'),
                                 'price': price.get('sells').get('unit_price')})
                                 
            sell_price = price.get('sells').get('unit_price')
            buy_price = price.get('buys').get('unit_price')
            profit = sell_price - buy_price - math.floor(sell_price * 0.15)
            details.update(profit=sell_price)

            # Save it
            firestore.put_item_data_to_db(details)
            items_written += 1
        except:
            print("Item has no data, skipping")

    return items_written


def is_api_return_valid(item_details: dict, item_prices: dict) -> bool:
    """
    Tests if the 2 input dicts are valid API returns.

    :param item_details:
    :param item_prices:
    :return:
    """
    # Check if we have a valid return object
    if "text" in item_details or "text" in item_prices:
        return False
    return True


def get_last_item_id() -> int:
    """
    Stores the lastt item in the array of all item IDs as the max item, so our loop knows when to reset.
    :return:
    """
    return gw2items.get_item_ids()[-1]


def update_meta_attr_after_run(meta_attrs: dict, last_processed_item: int):
    # Update our meta attributes so we don't rescan the same items every time
    current_time = datetime.datetime.now().strftime(_str_format)

    last_item_id = last_processed_item

    meta_attrs = {'last_run': current_time,
                  'last_item': last_item_id}

    firestore.put_meta_attributes(meta_attrs)
