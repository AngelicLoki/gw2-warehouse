import gw2_price_refresh.function_main

def main():
    """
     Main method application for when running the app locally for debugging.
     When running via CloudFunctions, refer to
     //TODO add python reference here when completed.
    """
    print("Running locally")
    function_main.main_method()

if __name__ == '__main__':
    main()
