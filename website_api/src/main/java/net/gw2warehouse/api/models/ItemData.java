package net.gw2warehouse.api.models;

import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.FieldPath;
import lombok.*;

@Getter
@NoArgsConstructor
@Setter
@AllArgsConstructor
@ToString
public class ItemData {
    public String name;
    public Long id;
    public Long profit;
    public PriceData buy;
    public PriceData sell;

    /**
     * Create an "ItemData" object from a firestore document
     * @param doc
     */
    public ItemData(DocumentSnapshot doc) {

        this.id = (Long) doc.get("id");
        this.name = (String) doc.get("name");
        this.profit = (Long) doc.get("profit");

        this.buy = new PriceData();
        buy.setPrice((Long) doc.get(FieldPath.of("buy","price")));
        buy.setQuantity((Long) doc.get(FieldPath.of("buy","quantity")));

        this.sell = new PriceData();
        sell.setPrice((Long) doc.get(FieldPath.of("sell","price")));
        sell.setQuantity((Long) doc.get(FieldPath.of("sell","quantity")));
    }
}
