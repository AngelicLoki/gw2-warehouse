package net.gw2warehouse.api.models;

import lombok.*;

@Getter
@NoArgsConstructor
@Setter
@AllArgsConstructor
@ToString
public class PriceData {
    public Long price;
    public Long quantity;
}
