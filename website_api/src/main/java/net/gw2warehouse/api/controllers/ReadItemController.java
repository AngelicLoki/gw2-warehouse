package net.gw2warehouse.api.controllers;

import com.google.api.core.ApiFuture;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.*;
import net.gw2warehouse.api.models.ItemData;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

@RestController
public class ReadItemController {

    private Firestore db = null;

    /**
     * Default constructor which initializes the DB connection.
     */
    public ReadItemController() {
        try {
            initdb();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Retrieve any given item by it'd item ID
     * @param id
     * @return
     * @throws ExecutionException
     * @throws InterruptedException
     */
    @GetMapping("/getItemById/{id}")
    public List<ItemData> getItemById(
            @PathVariable(value="id") Integer id
    ) throws ExecutionException, InterruptedException {

        List<ItemData> toReturn = new ArrayList<ItemData>();
        ApiFuture<QuerySnapshot> query = db.collection("items").whereEqualTo("id", id).get();
        for(DocumentSnapshot doc : query.get().getDocuments())
        {
            toReturn.add(new ItemData(doc));
        }

        return toReturn;
    }

    /**
     * Retrieve items that have the highest profit, allowing a profit limit to prevent single post items
     *
     * @param limit
     * @param offset
     * @return
     * @throws ExecutionException
     * @throws InterruptedException
     */
    @GetMapping("/getHighestProfitItems/")
    public List<ItemData> getHighestProfitItems(
            @RequestParam(name="limit", required = false, defaultValue = "20") Integer limit,
            @RequestParam(name="offset", required = false, defaultValue = "0") Integer offset,
            @RequestParam(name="profitLimit", required = false) Integer profitLimit
    ) throws ExecutionException, InterruptedException {

        //Build out the base query. Every parameter passed in here should be non-null or assigned a default value.
        List<ItemData> toReturn = new ArrayList<ItemData>();
        Query query = db.collection("items")
                .whereGreaterThan("profit", 0)
                .orderBy("profit", Query.Direction.DESCENDING)
                .limit(limit)
                .offset(offset);

        //If a profit limit is included, pass that in too
        if(profitLimit != null)
            query = query.whereLessThan("profit", profitLimit);

        //Run the actual Query
        ApiFuture<QuerySnapshot> queryFuture = query.get();
        for(DocumentSnapshot doc : queryFuture.get().getDocuments())
        {
            toReturn.add(new ItemData(doc));
        }

        return toReturn;
    }



    /**
     *  Initialize a firebase DB either from local credentials or from cloud credentials if the local credentials do
     *  not exist.
     * @throws IOException
     */
    private void initdb() throws IOException {
        // Check if we have a gcp_credentials object (which means we're running locally for testing)
        File credFile = new File("gcp_credentials.json");
        if(credFile.exists()){
            //We're initializing the DB locally
            FirestoreOptions firestoreOptions =
                    FirestoreOptions.getDefaultInstance().toBuilder()
                            .setProjectId("gw2-warehouse")
                            .setCredentials(GoogleCredentials.fromStream(new FileInputStream(credFile)))
                            .build();

            db = firestoreOptions.getService();
        }
        else {
            //We're initializing the DB locally
            FirestoreOptions firestoreOptions =
                    FirestoreOptions.getDefaultInstance().toBuilder()
                            .setProjectId("gw2-warehouse")
                            .setCredentials(GoogleCredentials.getApplicationDefault())
                            .build();

            db = firestoreOptions.getService();
        }
    }

}
