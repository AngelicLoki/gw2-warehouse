FROM docker:latest

RUN apk --no-cache add \
          curl \
          python3 \
          py3-crcmod \
          bash \
          libc6-compat \
          openssh-client \
          git \
          gnupg \
          && curl -O https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-290.0.1-linux-x86_64.tar.gz && \
          tar xzf google-cloud-sdk-290.0.1-linux-x86_64.tar.gz && \
          rm google-cloud-sdk-290.0.1-linux-x86_64.tar.gz && \
          ./google-cloud-sdk/bin/gcloud config set core/disable_usage_reporting true && \
          ./google-cloud-sdk/bin/gcloud config set component_manager/disable_update_check true && \
          ./google-cloud-sdk/bin/gcloud config set metrics/environment github_docker_image && \
          ./google-cloud-sdk/bin/gcloud --version

RUN apk --no-cache add openjdk11 --repository=http://dl-cdn.alpinelinux.org/alpine/edge/community

ENV PATH=$PATH:/google-cloud-sdk/bin/