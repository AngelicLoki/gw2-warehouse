# In GCP you have to enable a bunch of APIs in order to create resources. Enabled all required APIs here.
resource "google_project_service" "cloudbuild_api" {
  project = data.google_project.my_project.project_id
  service = "cloudbuild.googleapis.com"
}

resource "google_project_service" "scheduler_api" {
  project = data.google_project.my_project.project_id
  service = "cloudscheduler.googleapis.com"
}

resource "google_storage_bucket" "lambda-storage" {
  project = data.google_project.my_project.project_id
  name = "cloud-function-bucket-${split("/",data.google_project.my_project.id)[1]}"
  location = var.region

  storage_class = "STANDARD" # DR isn't super important right now
}

resource "google_storage_bucket_object" "lambda-object" {
  name = "price_lambda-${timestamp()}.zip"
  bucket = google_storage_bucket.lambda-storage.name
  source = "./my_function.zip"

  metadata = {
    date_uploaded = timestamp() # used to ensure the object is uploaded uniquely each time
  }

  # ZIP propogation is eventually consistent, so without this the downstream function has a race condition
  # where it will occassionnally retrieve the old ZIP.
  provisioner "local-exec" {
    command = "sleep 10"
  }
}

resource "google_pubsub_topic" "scheduler-sub" {
  name = "job-topic-pubsub-${split("/",data.google_project.my_project.id)[1]}"
}

resource "google_cloud_scheduler_job" "scheduled-job" {
  name = "price-scanner-job"
  description = "This job will periodically scan the GW2 API for price updates"
  schedule = "*/20 * * * *"

  pubsub_target {
    topic_name = google_pubsub_topic.scheduler-sub.id
    data       = base64encode("{}") # No data is required, the job has everything it needs
  }

  # Scheduler API has to be enabled before we can create this.
  depends_on = [google_project_service.scheduler_api]
}

resource "google_cloudfunctions_function" "function" {
  name        = "pricing-function"
  description = "Scans the GW2 API on a schedule to gather pricing data. Sha: ${filesha1("./my_function.zip")}"
  runtime     = "python37"

  available_memory_mb   = 128
  source_archive_bucket = google_storage_bucket.lambda-storage.name
  source_archive_object = google_storage_bucket_object.lambda-object.output_name
  entry_point           = "pubsub_entrypoint"

  event_trigger {
    event_type = "google.pubsub.topic.publish"
    resource = google_pubsub_topic.scheduler-sub.name
  }

  # Cloud Build API has to be enabled for cloud functions. For... reasons.
  depends_on = [google_project_service.cloudbuild_api]
}


