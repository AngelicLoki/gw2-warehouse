resource "google_project_service" "cloudrun_api" {
  project = data.google_project.my_project.project_id
  service = "run.googleapis.com"
}

resource "google_cloud_run_service" "api" {
  name     = var.cloudrun_name
  location = var.region

  template {
    spec {
      containers {
        image = "gcr.io/gw2-warehouse/api:${var.container_tag}"
      }
    }
  }

  traffic {
    percent         = 100
    latest_revision = true
  }

  autogenerate_revision_name = true
  depends_on = [google_project_service.cloudrun_api]
}

resource "google_cloud_run_service_iam_member" "allUsers" {
  service  = google_cloud_run_service.api.name
  location = google_cloud_run_service.api.location
  role     = "roles/run.invoker"
  member   = "allUsers"
}

