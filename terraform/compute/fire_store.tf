# In GCP you have to enable a bunch of APIs in order to create resources. Enabled all required APIs here.
resource "google_project_service" "cloud_resource_manager_api" {
  project = data.google_project.my_project.project_id
  service = "cloudresourcemanager.googleapis.com"
}
resource "google_project_service" "app_engine_api" {
  project = data.google_project.my_project.project_id
  service = "appengine.googleapis.com"
}
resource "google_project_service" "firestore_api" {
  project = data.google_project.my_project.project_id
  service = "firestore.googleapis.com"
}

# Firestore is tied to google app engine, so we have to create an app engine first.
resource "google_app_engine_application" "firestore_app" {
  project = data.google_project.my_project.project_id
  location_id = trim(var.region, "12345") # we just want the region without the number on the end

  database_type = "CLOUD_FIRESTORE"

  # Since there is no reference to the API creation, we have to add a manual
  #  dependency here.
  depends_on = [google_project_service.app_engine_api, google_project_service.firestore_api]
}
