
variable "zone" {
  default = "us-central1-a"
}

variable "region" {
  default = "us-central1"
}

variable "project_id" {
  default = "gw2-warehouse"
}

variable "cloudrun_name" {
  default = "gw2-warehouse-api"
}

variable "container_tag" {
  //No default, our CI job will pass this variable in
}