terraform {
  backend "gcs" {
    bucket  = "gw2-warehouse-terraform-backend"
    prefix  = "terraform/warehouse-container-state"
    credentials = "credentials.json"
  }
}

provider "google" {
  project = var.project_id
  region  = var.region
  zone    = var.zone
  credentials = file("credentials.json")
}

data "google_project" "my_project" {
  project_id = var.project_id
}