
variable "zone" {
  default = "us-central1-a"
}

variable "region" {
  default = "us-central1"
}

variable "project_id" {
  default = "gw2-warehouse"
}

variable "location" {
  default = "US"
}
